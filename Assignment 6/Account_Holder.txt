package bank.chennai;

/* Usage of access specifiers */

class SBI {
	String empName = "Meta Peace";
	String empId = "A24";
	String branch_name = "chennai";

	public static void main(String[] args) {

	}

	void get_loan(int amount) {
		System.out.println("The Sanctioned loan amount is " + amount);
	}

	void create_account() {
		System.out.println("Create Account method called ");
	}

}
